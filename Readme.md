
# Plan Marketing FullStack teste

  
#### Como instalar

- Na pasta do docker copiar '.env.example' e renomear como '.env'

- Na pasta do laravel copiar .env.example e renomear como como '.env' 

- executar o comando na pasta /Docker `docker-compose up -d --build`

- executar o comando na pasta /Docker `docker-compose exec php-apache composer install`

- executar o comando na pasta /Docker `docker-compose exec php-apache chmod -R 777 storage`

- executar o comando na pasta /Docker `docker-compose exec php-apache php artisan environment:start`

- executar o comando na pasta /Docker `docker-compose exec php-apache php artisan migrate:fresh --seed`

 #### Como utilizar

- link para acessar: [Quasar](localhost:8081/)
- link da api: [Laravel](http://localhost:8080/)
- link para acessar a documentação: [Swagger](http://localhost:8080/swagger/)
- O usuario e a senha já foram definidos na inicialização das variaveis (Usuario: "teste@mail.com", Senha: "secret" )

  
#### Sobre

- Foi utilizada o frameword Quasar devido a minha familiaridade com os componentes e minha experiência previa  
- Em minhas aplicações laravel sempre utilizei o Sanctum para autenticação, diante disso e do prazo, foi realizada somente uma implementação basica do JWT

#### Solução de erros comuns

- Daso o rewrite module não estiver ativado executar o comando `a2enmod rewrite` e reiniciar o apache

- .env do laravel deve conter:
  - DB_CONNECTION=pgsql
  - DB_HOST=postgres
  - DB_PORT=5432
  - DB_DATABASE=laravel
  - DB_USERNAME=postgres
  - DB_PASSWORD=secret
