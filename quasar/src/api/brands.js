import { makeRequest } from "./http";

export default {
  getBrands: () => makeRequest("get", "/brands"),
};
