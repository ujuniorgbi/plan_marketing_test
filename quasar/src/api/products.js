import { makeRequest } from "./http";

export default {
  getProducts: () => makeRequest("get", "/products"),
  getProduct: (id) => makeRequest("get", `/products/${id}`),
  createProduct: (data) => makeRequest("post", "/products", data),
  updateProduct: (id, data) => makeRequest("put", `/products/${id}`, data),
  deleteProduct: (id) => makeRequest("delete", `/products/${id}`),
};
