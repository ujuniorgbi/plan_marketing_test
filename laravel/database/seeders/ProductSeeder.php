<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Brand;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        $brands = Brand::all();

        foreach ($brands as $brand) {
            $product = new Product();
            $product->name = $this->generateProductName();
            $product->description = $this->generateProductDescription($product->name);
            $product->voltage = $faker->randomElement(['110', '220']);
            $product->brand_id = $brand->id;
            $product->save();
        }
    }

    private function generateProductName()
    {
        $products = [
            'Smartphone',
            'Notebook',
            'Televisão',
            'Fones de ouvido',
            'Câmera',
        ];
        return $products[array_rand($products)];
    }

    private function generateProductDescription($productName)
    {
        return "Este é um $productName de alta qualidade";
    }

    private function generateProductVoltage()
    {
        return rand(110, 220);
    }
}

