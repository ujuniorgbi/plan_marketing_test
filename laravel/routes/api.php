<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BrandController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::get('me', [AuthController::class, 'me'])->middleware('apiJWT');
    Route::get('logout', [AuthController::class, 'logout'])->middleware('apiJWT');
    Route::get('refresh', [AuthController::class, 'refresh'])->middleware('apiJWT');
});

Route::middleware(['apiJWT'])->group(function () {
    Route::resource('products', ProductController::class);
    Route::get('brands', [BrandController::class, 'index']);
});
