<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Postgres extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'postgres:createdb {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new postgres database schema based on the database config file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schemaName = $this->argument('name') ?: config("database.connections.pgsql.database");
        $charset = config("database.connections.pgsql.charset",'utf8');

        config(["database.connections.pgsql.database" => null]);

        $query = "CREATE DATABASE $schemaName ENCODING $charset";

        DB::statement($query);

        config(["database.connections.pgsql.database" => $schemaName]);

    }
}
