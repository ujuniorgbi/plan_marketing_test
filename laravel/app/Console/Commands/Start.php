<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Start extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'environment:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'prepare the environment to use';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Generate jwt secret');
        Artisan::call('jwt:secret');
        $this->info('Generate Key');
        Artisan::call('key:generate');
        $this->info('Generate Postgres database');
        Artisan::call('postgres:createdb');
        
        return Command::SUCCESS;
    }
}
