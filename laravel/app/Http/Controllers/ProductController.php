<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\GetProductsRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{

    /**
     * Get products with optional filters.
     *
     * @param GetProductsRequest $request
     * @return JsonResponse
     *
     * @OA\Get(
     *     tags={"Produtos"},
     *     path="/api/products",
     *     summary="Listar produtos",
     *     description="Recupera uma lista de produtos com filtros opcionais.",
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Nome do produto (filtro opcional).",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="voltage",
     *         in="query",
     *         description="Voltagem do produto (filtro opcional).",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="brand",
     *         in="query",
     *         description="Marca do produto (filtro opcional).",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso: retorna a lista de produtos."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Erro de autenticação: o usuário não está autenticado."
     *     )
     * )
     */
    public function index(GetProductsRequest  $request): JsonResponse
    {
        $query = Product::with('brand');

        if ($request->filled('name')) {
            $query->where('name', 'LIKE', '%' . $request->input('name') . '%');
        }
        if ($request->filled('voltage')) {
            $query->where('voltage', $request->input('voltage'));
        }
        if ($request->filled('brand')) {
            $query->whereHas('brand', function ($q) use ($request) {
                $q->where('name', $request->input('brand'));
            });
        }

        $products = $query->orderBy('id')->get();

        return response()->json($products);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/products/{id}",
     *     tags={"Produtos"},
     *     summary="Obter produto",
     *     description="Recupera as informações de um produto específico.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID do produto.",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso: retorna as informações do produto."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Erro de autenticação: o usuário não está autenticado."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Erro: o produto não foi encontrado."
     *     )
     * )
     */
    public function show(int $id): JsonResponse
    {
        $product = Product::with('brand')->findOrFail($id);
        return response()->json($product);
    }

   /**
     * Create a new product.
     *
     * @param CreateProductRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     path="/api/products",
     *     tags={"Produtos"},
     *     summary="Criar produto",
     *     description="Cria um novo produto.",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Nome do produto"),
     *             @OA\Property(property="price", type="number", format="float", example=9.99),
     *             @OA\Property(property="description", type="string", example="Descrição do produto"),
     *             @OA\Property(property="brand_id", type="integer", example=1)
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Sucesso: retorna as informações do produto criado."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Erro de autenticação: o usuário não está autenticado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Erro de validação: ocorreu um erro na validação dos dados enviados."
     *     )
     * )
     */
    public function store(CreateProductRequest $request): JsonResponse
    {
        $product = Product::create($request->validated());
        return response()->json($product, 201);
    }

    /**
     * Update a product.
     *
     * @param UpdateProductRequest $request
     * @param int $id
     * @return JsonResponse
     *
     * @OA\Put(
     *     path="/api/products/{id}",
     *     tags={"Produtos"},
     *     summary="Atualizar produto",
     *     description="Atualiza as informações de um produto existente.",
     *     @OA\Parameter(
     *         name="id",
     *         description="ID do produto",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Nome atualizado do produto"),
     *             @OA\Property(property="price", type="number", format="float", example=19.99),
     *             @OA\Property(property="description", type="string", example="Descrição atualizada do produto"),
     *             @OA\Property(property="brand_id", type="integer", example=2)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso: retorna as informações do produto atualizado."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Erro de autenticação: o usuário não está autenticado."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Erro: o produto não foi encontrado."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Erro de validação: ocorreu um erro na validação dos dados enviados."
     *     )
     * )
     */
    public function update(UpdateProductRequest $request, int $id): JsonResponse
    {
        $product = Product::findOrFail($id);
        $product->update($request->validated());
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/products/{id}",
     *     tags={"Produtos"},
     *     summary="Excluir produto",
     *     description="Exclui um produto existente.",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID do produto.",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Sucesso: produto excluído com sucesso."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Erro de autenticação: o usuário não está autenticado."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Erro: o produto não foi encontrado."
     *     )
     * )
     */
    public function destroy(int $id): JsonResponse
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json(null, 204);
    }

}
