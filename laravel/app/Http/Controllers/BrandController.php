<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{

     /**
     * @OA\Get(
     *     path="/api/brands",
     *     tags={"Marcas"},
     *     summary="Obter todas as marcas",
     *     description="Obtém uma lista de todas as marcas disponíveis.",
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso: retorna uma lista de marcas."
     *     )
     * )
     */
    public function index()
    {
        $brands = Brand::all();
        return response()->json($brands);
    }
}
