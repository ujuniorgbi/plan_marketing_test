<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'string|max:255',
            'description' => 'string',
            'voltage' => 'in:110,220',
            'brand_id' => 'exists:brands,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.string' => 'O nome deve ser uma string.',
            'name.max' => 'O nome deve ter no máximo :max caracteres.',
            'description.string' => 'A descrição deve ser uma string.',
            'voltage.in' => 'A voltagem deve ser 110v ou 220v.',
            'brand_id.exists' => 'A marca selecionada é inválida.',
        ];
    }
}
