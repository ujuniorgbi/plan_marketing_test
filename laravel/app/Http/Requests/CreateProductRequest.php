<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'voltage' => 'required|in:110,220',
            'brand_id' => 'required|exists:brands,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => 'O campo nome é obrigatório.',
            'name.string' => 'O campo nome deve ser uma string.',
            'name.max' => 'O campo nome deve ter no máximo :max caracteres.',
            'description.required' => 'O campo descrição é obrigatório.',
            'description.string' => 'O campo descrição deve ser uma string.',
            'voltage.required' => 'O campo tensão é obrigatório.',
            'voltage.in' => 'O campo tensão deve ser 110 ou 220.',
            'brand_id.required' => 'O campo marca é obrigatório.',
            'brand_id.exists' => 'A marca selecionada é inválida.',
        ];
    }
}
